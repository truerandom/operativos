#include <stdio.h>
#include <string.h>
void inversa(char cadena[]){
        int limite=(strlen(cadena))/2;
        if(strlen(cadena)%2==1)
                limite=limite+1;
        int i=0;
        for(i=0;i<limite;i++){
                char izq=cadena[i];
                char der=cadena[strlen(cadena)-(i+1)];
                cadena[strlen(cadena)-(i+1)]=izq;
                cadena[i]=der;
        }
}

int main(int argc,char *args[]){
	if(argc > 1){
		inversa(args[1]);
		printf("%s\n",args[1]);
	}
	else
		printf("\nUsage:\n./inversa 'string'\n");
	return 0;
}
