#include <stdio.h>
#include <time.h>   //ctime
#include <limits.h> //constantes:= INT_MAX.

int main(){
  time_t limite = INT_MAX; //Asigna el valor maximo de un entero a tiempo
  printf("Fin del mundo: %s\n", ctime(&limite)); //Imprime la fecha 
  return 0;
}
