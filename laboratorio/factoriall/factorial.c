#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
int factorialrecursivo(int n){
	if(n<1)
		return 1;
	else
		return n*factorialrecursivo(n-1);
}

int main(int argc,char *args[]){
	int num = 3;
	printf("%d\n",factorialrecursivo(num));
	return 0;
}
