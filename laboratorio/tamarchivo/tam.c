#include<stdio.h>
#include<fcntl.h>
#include<sys/types.h>
#include<unistd.h>
int main(int argc, char*argv[]){
	if(argc == 2){
		//descriptor,nread
		int fd, nread;
		//Abre el archivo con permisos de lectura
		fd = open(argv[1],O_RDONLY);
		if(fd==-1)
			printf("Error de lectura");
		//vamos al final del archivo, contamos bytes
		nread = lseek(fd,0,SEEK_END);
		close(fd);
		printf("\nArchivo\n"
			"Bits: %d\t"
			"Kbs: %d\t"
			"Megas: %d\t",
			nread,nread/1000,(nread/1000)/1000);
		return 0;
	}
	printf("\nNecesitas pasar la ruta de un archivo\n");
	return 0;
}
